// Extract domain name (DN) from URL
function url2dn(url) {
	var tmpa = document.createElement('a');
	tmpa.href = url;
	return tmpa.host;
}

// maintain a dict of IPs, indexed by DN
var ips = {};
var dds = {};
chrome.webRequest.onCompleted.addListener(
	function (d) {
		dds[url2dn(d.url)] = d;
		ips[url2dn(d.url)] = d.ip;
		return;
	}, {
		urls: [],
		types: []
	}, []
);

if (localStorage["ext_enabled"] === undefined)
	localStorage.setItem("ext_enabled", 1);

// Listeners
chrome.extension.onMessage.addListener(
	function (request, sender, callback) {
		//console.info(request.op);
		switch (request.op) {
			case "enable":
				localStorage.setItem("ext_enabled", 1);
				break;

			case "disable":
				localStorage.setItem("ext_enabled", 0);
				break;

			case "showTip":
				callback({
					ipaddress: localStorage["ipaddress"],
					ext_enabled: localStorage["ext_enabled"]
				});
				break;

			case "getIp":
				var dn = url2dn(sender.tab.url);
				var iIp = ips[dn];
				callback({
					ip: iIp,
					dds: dds,
				});
				break;

			case "getAddr":
				var ipaddress = "";
				$.ajax({
					url: "https://clientapi.ipip.net/browser/chrome?ip=" + request.ip,
					timeout: 2000,
					type: 'get',
					data: {},
					dataType: 'json',
					success: function (info) {
						if (info.ret == 0) {
							if (info.data.city != "") { ipaddress = info.data.country + '-' + info.data.city; }
							else {
								ipaddress = info.data.country
							}
							localStorage.setItem("ipaddress", ipaddress);
						} else {
							localStorage.setItem("ipaddress", '');
						}
					},
					error: function (XMLHttpRequest, textStatus, errorThrown) {
						localStorage.setItem("ipaddress", '');
					},
					complete: function (XMLHttpRequest, status) {
						if (status == 'timeout') {
							//ajaxTimeoutTest.abort();
							localStorage.setItem("ipaddress", '');
						}
					}
				});

				callback({
					ip: request.ip,
					ipaddress: localStorage["ipaddress"],
					ext_enabled: localStorage["ext_enabled"]
				});

				break;

			default:
				break;
		}
	}
);

chrome.browserAction.onClicked.addListener(function (t) {
	e = localStorage["ext_enabled"];
	if (e == 1) {
		iconDetails = {
			path: "/images/icon19_disable.png"
		};

	} else {
		iconDetails = {
			path: "/images/icon19.png"
		};
	}
	chrome.browserAction.setIcon(iconDetails, null);
	localStorage["ext_enabled"] = 1 - e;
});
