var appHoverTitle = chrome.i18n.getMessage('appHoverTitle');

function placeIPDiv() {
  chrome.extension.sendMessage({
    op: "getIp"
  }, function (response) {
    var ip = response.ip;
    var dds = response.dds;

    // inject 和 background 不互通 session, option 和 background 互通 session
    // 根据ip 获取所在地址
    chrome.extension.sendMessage({
      op: "getAddr",
      ip: ip
    }, function (response) {
      setTimeout(function () {
        chromeShowTip(ip)
      }, 1000);
    });
  });
}

function chromeShowTip(ip) {
  chrome.extension.sendMessage({
    op: "showTip"
  }, function (response) {
    var ext_enabled = response.ext_enabled;
    var ipaddress = response.ipaddress;

    if (ext_enabled == 1 || ext_enabled === undefined) {
      $("body").append('<div title="' + appHoverTitle +
        '" id="chrome-website-ip" >' + ipaddress + '<div id="chrome-website-ip-value">' + ip + "</div>" +
        '</div>');
      // 点击copy
      $("#chrome-website-ip").on('click', function () {
        copy("chrome-website-ip-value");
        $("#chrome-website-ip-value").text("Copied!")
        setTimeout(function () { $("#chrome-website-ip-value").text(ip) }, 1500);
      });

      chrome.storage.sync.get({
        autohidden: true
      }, function (items) {
        if (items.autohidden) {
          setTimeout(function () { document.getElementById("chrome-website-ip").style.display = "none"; }, 3000);
        }
      });
    }
  });
};

function copy(id) {
    //Copy Content
    var text = $("#"+id).text()
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();
}

function hideIPDiv() {
  document.getElementById('chrome-website-ip').remove();
}

$(document).ready(placeIPDiv);
window.lastKeyCode = 0;
window.lastHitTime = new Date();

$(document).keyup(function (e) {
  // If the 'Esc' key is pressed before the HTML (yes, HTML only) could
  // fully load, show the IP <div> as $(document).ready() doesn't execute.
  if (document.getElementById('chrome-website-ip') === null && e.keyCode ==
    27) {
    placeIPDiv();
  } else {
    now = new Date();
    timeDiff = now - window.lastHitTime;
    window.lastHitTime = now;

    //  双击`F2`弹出可复制的`ip`
    /*        if (e.keyCode == 113 && window.lastKeyCode == 113 &&  // two "F2" keystrokes
                timeDiff < 900) {             // within 900ms
                window.lastKeyCode = 0;
                window.prompt('IP of "' + window.location.host + '":',
                    document.getElementById('chrome-website-ip').innerText);
            }*/

    //  双击`F4` 去项目主页
    if (e.keyCode == 115 && window.lastKeyCode == 115 && timeDiff < 900) {
      window.lastKeyCode = 0;
      window.open(
        "http://git.oschina.net/surprise/Chrome.Website.Ip#git-readme");
    }

    // 再次按ESC隐藏
    if (e.keyCode == 27) {
      hideIPDiv();
    }
  }
  window.lastKeyCode = e.keyCode;
});
