# 显示网站 ip WebsiteIP

主要有几个场景:

1. 平时修改了 host 切换 ip 后,不知道访问的站点 ip
2. 想知道当前站的 ip

演示图片
![demo](http://git.oschina.net/surprise/Chrome.Website.Ip/raw/master/images/demo.png)

## 下载地址

[chrome 商店](https://chrome.google.com/webstore/detail/eaghlkamibfjbicomdmbbohljdhicpgb)

## 快捷键说明

快捷键：

按`ESC`切换展示和隐藏左下角`IP`

双击地址复制 IP

## 无法打开商店如何安装

点击`chrome`浏览器最右侧的按钮（就是最右边的按钮嘛）-> 工具 -> 扩展程序 （或者地址栏输入`chrome://extensions/`）

`chrome`插件同样也可以用于`360急速`、`猎豹浏览器`
